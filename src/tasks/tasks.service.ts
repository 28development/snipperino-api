import {Injectable, Logger} from '@nestjs/common';
import {Cron, CronExpression} from "@nestjs/schedule";

@Injectable()
export class TasksService {
  private readonly logger = new Logger(TasksService.name);

  @Cron('45 * * * * *')
  handleCron() {
    this.logger.debug('Called every 45 seconds');
  }

  @Cron(CronExpression.EVERY_5_MINUTES)
  handleCoolCron() {
    this.logger.debug('Called every 5 Minutes');
  }
}
