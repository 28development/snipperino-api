import { Module } from '@nestjs/common';
import { UsersService } from './users.service';

@Module({
  providers: [UsersService],
  exports: [UsersService] // make it visible outside of the module, used in AuthService
})
export class UsersModule {}
