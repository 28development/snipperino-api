import { ValidationPipe } from './common/pipes/validation.pipe';
import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';

async function bootstrap() {
	const app = await NestFactory.create(AppModule);

	// swagger / open api setup
	const options = new DocumentBuilder()
		.setTitle('Reader API')
		.setDescription('Reader API')
		.setVersion('1.0')
		.addTag('reader api')
		.build();
	const document = SwaggerModule.createDocument(app, options);
	SwaggerModule.setup('api', app, document);

	// global pipes are used across the whole application, for every controller and every route handler
	app.useGlobalPipes(new ValidationPipe());
	await app.listen(3000);
	console.log(`Application is running on: ${await app.getUrl()}`);
}
bootstrap();
