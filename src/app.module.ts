import { CatsModule } from './cats/cats.module';
import { CoreModule } from './core/core.module';
import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { AuthModule } from './auth/auth.module';
import { UsersModule } from './users/users.module';
import { AppController } from './app.controller';
import { ImageuploadsModule } from './imageuploads/imageuploads.module';
import { ScheduleModule } from '@nestjs/schedule';
import { TasksService } from './tasks/tasks.service';

// @todo put into .env file
// const mongo_uri = 'mongodb+srv://utdevadmin:t1rX7SmKtL65Fc3D@cluster0-lbutk.mongodb.net/nestjs-test?retryWrites=true&w=majority';
// const mongo_uri = 'mongodb+srv://root:ut22992@nestjstestapp.mbynb.mongodb.net/NestJsTestApp?retryWrites=true&w=majority';
const mongo_uri = 'mongodb://localhost/cats';

// root module
@Module({
	controllers: [AppController],
	imports: [
		MongooseModule.forRoot(mongo_uri),
		ScheduleModule.forRoot(),
		CoreModule,
		CatsModule,
		AuthModule,
		UsersModule,
		ImageuploadsModule
	],
	providers: [TasksService]
})
export class AppModule{}
