import { Module } from '@nestjs/common';
import { ImageuploadsController } from './imageuploads.controller';
import {ImageuploadsService} from './imageuploads.service';

@Module({
  controllers: [ImageuploadsController],
  providers: [ImageuploadsService],
  exports: [ImageuploadsService]
})

export class ImageuploadsModule {}