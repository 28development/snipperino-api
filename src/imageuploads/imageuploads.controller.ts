import { Controller, Post, Req, Res } from '@nestjs/common';
import {ImageuploadsService} from './imageuploads.service';

@Controller('fileupload')
export class ImageuploadsController {
  constructor(private readonly imageUploadService: ImageuploadsService) {}

  @Post()
  async create(@Req() request, @Res() response) {
    try {
      await this.imageUploadService.fileupload(request, response);
    } catch (error) {
      return response
              .status(500)
              .json(`Failed to upload image file: ${error.message}`);
    }
  }
}