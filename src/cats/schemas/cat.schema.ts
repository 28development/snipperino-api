import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose'
import { Document } from 'mongoose'

// Der decorator @Schema() markiert die Klasse als Schemadefinition.
// Es ordnet unsere Cat-Klasse einer gleichnamigen MongoDB-Sammlung zu.
// (fügt jedoch am Ende ein s hinzu -> Plural -> cat -> cats)
@Schema()
export class Cat extends Document {
  // der Prop decorator definiert eine Eigenschaft im Dokument.
  // Die Schematypen werden dank Typescript-MetadatenFunktionen automatisch abgeleitet.
  @Prop()
  name: string;

  @Prop()
  age: number;

  @Prop()
  breed: string;
}

export const CatSchema = SchemaFactory.createForClass(Cat);
