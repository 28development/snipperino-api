import {Injectable} from '@nestjs/common'
import {InjectModel} from '@nestjs/mongoose'
import {Model} from 'mongoose'
import {CreateCatDto} from './dto/create-cat.dto'
import {Cat} from './schemas/cat.schema'

@Injectable()
export class CatsService {
  constructor(@InjectModel('Cat') private readonly catModel: Model<Cat>) {}

  async create(createCatDto: CreateCatDto): Promise<Cat> {
    const createdCat = new this.catModel(createCatDto);
    
    return createdCat.save();
  }

  async findAll(): Promise<Cat[]> {
    return await this.catModel.find().exec();
  }

  async findOne(id: string): Promise<Cat> {
    return await this.catModel.findById(id).exec();
  }

  async update(id: string, createCatDto: CreateCatDto): Promise<Cat> {
    return this.catModel.findByIdAndUpdate(id, createCatDto, {new: true});
  }

  async delete(id: string): Promise<Cat> {
    return this.catModel.findByIdAndRemove(id);
  }
}