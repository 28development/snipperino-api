import {
  Body,
  Controller,
  Delete,
  Get,
  HttpStatus,
  NotFoundException,
  Param,
  Patch,
  Post,
  Put,
  Res,
  UseGuards
} from '@nestjs/common'
import { Roles } from '../common/decorators/roles.decorator'
import { CatsService } from './cats.service'
import { CreateCatDto } from './dto/create-cat.dto'
import { Cat } from './interfaces/cat.interface'
import {JwtAuthGuard} from "../auth/jwt-auth.guard";

@UseGuards(JwtAuthGuard)
@Controller('cats')
export class CatsController {
  constructor(private readonly catsService: CatsService) {}

  @Get()
  async findAll(): Promise<Cat[]> {
    return this.catsService.findAll();
  }

  @Get(':id')
  async findOne(@Param('id') id: string): Promise<Cat> {
    const cat = this.catsService.findOne(id);

    if (!cat) {
      throw new NotFoundException(`Cat with the id of ${id} does not exist!`)
    }

    return cat;
  }

  // only the user role admin shall be able to call this route,
  // typically the role is sent within the header
  @Post()
  @Roles('admin')
  async create(@Res() res, @Body() createCatDto: CreateCatDto): Promise<Cat> {
    const cat = this.catsService.create(createCatDto);

    return res.status(HttpStatus.OK).json({
      message: 'Cat has been successfully created!',
      cat,
    });
  }

  @Patch(':id')
  async updateProduct(id: string, @Body() createCatDto: CreateCatDto): Promise<Cat> {
    return this.catsService.update(id, createCatDto);
  }

  @Delete(':id')
  async delete(@Param('id') id: string): Promise<Cat> {
    const cat = this.catsService.delete(id);

    if (!cat) {
      throw new NotFoundException(`Cat with the id of ${id} does not exist!`);
    }

    return cat;
  }
}
