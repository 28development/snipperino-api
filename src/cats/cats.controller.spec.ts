import { Test } from '@nestjs/testing';
import { CatsController } from './cats.controller';
import { CatsService } from './cats.service';
import { Cat } from './interfaces/cat.interface';
import * as mongoose from "mongoose";
import * as mocks from 'node-mocks-http';

const DB = process.env.DB || "mongodb://localhost/cats";
mongoose.connect(DB, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useCreateIndex: true,
  useFindAndModify: false
});

describe('CatsCrud', () => {
  let catsController: CatsController;
  let catsService: CatsService;

  beforeEach(async () => {
    const moduleRef = await Test.createTestingModule({
      controllers: [CatsController],
      providers: [CatsService],
    }).compile();

    catsService = moduleRef.get<CatsService>(CatsService);
    catsController = moduleRef.get<CatsController>(CatsController);
  });

  describe('create', () => {
    it('should create a cat', async () => {
      const cat: Cat = {
        name: "Robert",
        age: 24,
        breed: "Stark"
      }

      const req = mocks.createRequest();
      const res = req.createResponse();

      expect(await catsController.create(res, cat)).toHaveProperty('_id');
    })
  });

  describe('findOne', () => {
    it('should return a cat with an id of', async () => {
      const id: string = '5f0850acc6e4ae4bad559ffa';
      const cat: Cat = {
        name: "Ar",
        age: 12,
        breed: "wolf",
      }

      //jest.spyOn(catsService, 'findOne').mockResolvedValue(cat)
      //jest.spyOn(catsService, 'findOne').mockImplementation(() => cat);
      expect(await catsController.findOne(id)).toBe(cat);
    })
  });

  describe('findAll', () => {
    it('should return an array of cats', async () => {
      const result: Cat[] = [
        {
          age: 2,
          breed: 'Bombay',
          name: 'Pixel',
        },
      ];
      //jest.spyOn(catsService, 'findAll').mockImplementation(() => result);

      expect(await catsController.findAll()).toBe(result);
    });
  });

  afterAll(() => {
    mongoose.disconnect();
  });
});
