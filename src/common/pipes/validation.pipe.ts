import {
  ArgumentMetadata,
  BadRequestException,
  Injectable,
  PipeTransform,
  Type,
} from '@nestjs/common';
import { plainToClass } from 'class-transformer';
import { validate } from 'class-validator';

@Injectable()
export class ValidationPipe implements PipeTransform<any> {
  async transform(value: any, metadata: ArgumentMetadata): Promise<any> {
    const { metatype } = metadata;
    if (!metatype || !this.toValidate(metatype)) {
      return value;
    }
    // transform js argument object into a typed object so we can apply validation,
    // because the incoming post body object, when deserialized from the network request,
    // does not have any type information
    const object = plainToClass(metatype, value);
    const errors = await validate(object);

    if (errors.length > 0) {
      throw new BadRequestException(errors);
    }

    return value;
  }

  // helper function to check if the current argument is a native JavaScript type
  // (these can't have validation decorators)
  private toValidate(metatype: Type<any>): boolean {
    const types = [String, Boolean, Number, Array, Object];

    // if the metatype is not included in the array return true else false
    return !types.find(type => metatype === type);
  }
}
