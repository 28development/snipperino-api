import {
  ArgumentsHost,
  Catch,
  ExceptionFilter,
  HttpException,
} from '@nestjs/common';

// The @Catch(HttpException) decorator binds the required metadata to the exception filter,
// telling Nest that this particular filter is looking for exceptions of type HttpException and nothing else.
// The @Catch() decorator may take a single parameter, or a comma - separated list.
// This lets you set up the filter for several types of exceptions at once.
@Catch(HttpException)
export class HttpExceptionFilter implements ExceptionFilter<HttpException> {
  // The exception parameter is the exception object currently being processed. The host parameter is an ArgumentsHost object
  // Argumentshost is used to get a obtain a reference to the Request and Response objects,
  // that are being passed to the original request handler
  catch(exception: HttpException, host: ArgumentsHost) {
    const ctx = host.switchToHttp();
    const response = ctx.getResponse();
    const request = ctx.getRequest();
    const statusCode = exception.getStatus();

    response.status(statusCode).json({
      statusCode,
      timestamp: new Date().toISOString(),
      path: request.url,
    });
  }
}
